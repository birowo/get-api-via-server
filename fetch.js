function root(req, res, url){
  res.setHeader('Content-Type', 'text/html; charset=utf-8')
  res.end(`
<html>
<head>
</head>
<body>
<div id="apires"></div>
<script>
fetch('/indonesia/provinsi')
  .then(response => response.json())
  .then(data => { 
    apires.innerHTML = JSON.stringify(data, undefined, '..').replace(/\\n\\.+/g, x => '<br>'+'&nbsp;'.repeat(x.length-1))
  })
</script>
</body>
</html>
  `)
}
const https = require('https')
function api(req, res, url){
  res.setHeader('Content-Type', 'application/json')
  const apireq = https.request('https://api.kawalcorona.com/indonesia/provinsi/', apires => {
    apires.pipe(res)
  })
  apireq.on('error', err => { console.log(err) })
  apireq.end()
}
function notfound(req, res, url){
  res.statusCode = 404
  res.end('not found')
}
const route = {
  '/': root,
  '/indonesia/provinsi': api
}

require('http').createServer((req, res)=>{
  const url = req.url.split('?');
  (route[url[0]] || notfound)(req, res, url)
}).listen(8080)